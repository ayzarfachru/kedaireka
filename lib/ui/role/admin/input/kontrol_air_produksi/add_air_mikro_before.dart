import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/auth/regist.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/admin/input/bahan_baku/add_success.dart';
import 'package:kedaireka/ui/role/admin/input/kontrol_air_produksi/add_air_kimiawi_after.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class AddAirMikroB extends StatefulWidget {
  const AddAirMikroB({Key? key}) : super(key: key);

  @override
  _AddAirMikroBState createState() => _AddAirMikroBState();
}

class _AddAirMikroBState extends State<AddAirMikroB> {

  final TextEditingController fieldALT = TextEditingController(text: "");
  final TextEditingController fieldKapang = TextEditingController(text: "");
  final TextEditingController fieldColiform = TextEditingController(text: "");
  final TextEditingController fieldSalmonella = TextEditingController(text: "");
  final TextEditingController fieldAureus = TextEditingController(text: "");
  final TextEditingController fieldKhamir = TextEditingController(text: "");
  final TextEditingController fieldEColi = TextEditingController(text: "");

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 24),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Data Kontrol Air Produksi ------------------------------------
                          Container(
                            // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                text: "Data Kontrol Air Produksi\nBefore",
                                maxLines: 2,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan data kualitas kontrol air yang",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "diguanakan saat produksi",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                // color: CustomColor.primaryLight2,
                                border: Border.all(color: CustomColor.primaryLight2)
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 20, vertical: CustomSize.sizeWidth(context) / 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  CustomText.textTitle18(
                                    text: "Uji Mikrobiologi",
                                    maxLines: 1,
                                    color: CustomColor.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.02,
                                  ),

                                  // ------------------------------------ Analisis ALT (Angka Lempeng Total) ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis ALT (Angka Lempeng Total)",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis ALT (Angka Lempeng Total) ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldALT,
                                    hintText: 'Angka Lempeng Total',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis Kapang ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis Kapang",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis Kapang ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKapang,
                                    hintText: 'Analisis Kapang',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis Coliform ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis Coliform",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis Coliform ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldColiform,
                                    hintText: 'Analisis Coliform',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis Salmonella ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis Salmonella",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis Salmonella ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldSalmonella,
                                    hintText: 'Analisis Salmonella',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis S. Aureus ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis S. Aureus",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis S. Aureus ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldAureus,
                                    hintText: 'Analisis S. Aureus',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis Khamir ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis Khamir",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis Khamir ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKhamir,
                                    hintText: 'Analisis Khamir',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Analisis E. Coli ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis E. Coli",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Analisis E. Coli ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldEColi,
                                    hintText: 'Analisis E. Coli',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          // ------------------------------------ Button ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.theme,
                                      alignment: Alignment.center,
                                      duration: const Duration(milliseconds: 700),
                                      child: AddAirKimiawiA()));
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Simpan',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
