import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/auth/regist.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/admin/input/bahan_baku/add_success.dart';
import 'package:kedaireka/ui/role/admin/input/kontrol_air_produksi/add_air_mikro_before.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';

import 'add_air_mikro_after.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class AddAirKimiawiA extends StatefulWidget {
  const AddAirKimiawiA({Key? key}) : super(key: key);

  @override
  _AddAirKimiawiAState createState() => _AddAirKimiawiAState();
}

class _AddAirKimiawiAState extends State<AddAirKimiawiA> {

  final TextEditingController fieldPhAir = TextEditingController(text: "");
  final TextEditingController fieldCadmium = TextEditingController(text: "");
  final TextEditingController fieldKadarLead = TextEditingController(text: "");
  final TextEditingController fieldBeratTin = TextEditingController(text: "");
  final TextEditingController fieldMercury = TextEditingController(text: "");
  final TextEditingController fieldArsenic = TextEditingController(text: "");
  final TextEditingController fieldH2O2 = TextEditingController(text: "");
  final TextEditingController fieldKadarNitrit = TextEditingController(text: "");

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 24),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Data Kontrol Air Produksi ------------------------------------
                          Container(
                            // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                text: "Data Kontrol Air Produksi\nAfter",
                                maxLines: 2,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan data kualitas kontrol air yang",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "diguanakan saat produksi",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                // color: CustomColor.primaryLight2,
                                border: Border.all(color: CustomColor.primaryLight2)
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 20, vertical: CustomSize.sizeWidth(context) / 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  CustomText.textTitle18(
                                    text: "Uji Kimiawi",
                                    maxLines: 1,
                                    color: CustomColor.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.02,
                                  ),

                                  // ------------------------------------ PH Air ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   PH Air",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field PH Air ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldPhAir,
                                    hintText: 'Ph Air',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Cadmium ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Logam Berat Cadmium",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Cadmium ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldCadmium,
                                    hintText: 'Kadar Cadmium',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Lead ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Logam Berat Lead",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Lead ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKadarLead,
                                    hintText: 'Kadar Lead',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Berat Tin ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Logam Berat Tin",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Berat Tin ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldBeratTin,
                                    hintText: 'Kadar Tin',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Berat Mercury ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Logam Berat Mercury",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Berat Mercury ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldMercury,
                                    hintText: 'Kadar Mercury',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Kadar Logam Berat Arsenic ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Logam Berat Arsenic",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Logam Berat Arsenic ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldArsenic,
                                    hintText: 'Kadar Arsenic',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ H2O2 ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   H2O2",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field H2O2 ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldH2O2,
                                    hintText: 'H2O2 Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Nitrit ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Nitrit",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Nitrit ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKadarNitrit,
                                    hintText: 'Kadar nitrit Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          // ------------------------------------ Button ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.theme,
                                      alignment: Alignment.center,
                                      duration: const Duration(milliseconds: 700),
                                      child: AddAirMikroA()));
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Simpan',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
