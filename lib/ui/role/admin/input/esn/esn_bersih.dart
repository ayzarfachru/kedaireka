import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/auth/regist.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/admin/input/bahan_baku/add_success.dart';
import 'package:kedaireka/ui/role/admin/input/kontrol_air_produksi/add_air_kimiawi_after.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class AddEsnBersih extends StatefulWidget {
  const AddEsnBersih({Key? key}) : super(key: key);

  @override
  _AddEsnBersihState createState() => _AddEsnBersihState();
}

class _AddEsnBersihState extends State<AddEsnBersih> {

  final TextEditingController fieldKadarNitrit = TextEditingController(text: "");
  final TextEditingController fieldH2O2 = TextEditingController(text: "");
  final TextEditingController fieldLemak = TextEditingController(text: "");
  final TextEditingController fieldProtein = TextEditingController(text: "");
  final TextEditingController fieldKarbohidrat = TextEditingController(text: "");
  final TextEditingController fieldAir = TextEditingController(text: "");
  final TextEditingController fieldAbu = TextEditingController(text: "");

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 24),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Data Kontrol Air Produksi ------------------------------------
                          Container(
                            // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                text: "Data ESN Putih Bersih",
                                maxLines: 2,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan data Data ESN Putih Kotor",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                // color: CustomColor.primaryLight2,
                                border: Border.all(color: CustomColor.primaryLight2)
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 20, vertical: CustomSize.sizeWidth(context) / 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  CustomText.textTitle18(
                                    text: "Uji Kimiawi",
                                    maxLines: 1,
                                    color: CustomColor.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.02,
                                  ),

                                  // ------------------------------------ Nitrit ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Nitrit",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Nitrit ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKadarNitrit,
                                    hintText: 'Kadar nitrit Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ H2O2 ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   H2O2",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field H2O2 ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldH2O2,
                                    hintText: 'H2O2 Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.02,
                                  ),

                                  CustomText.textTitle18(
                                    text: "Uji Nutrisi",
                                    maxLines: 1,
                                    color: CustomColor.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.02,
                                  ),

                                  // ------------------------------------ Kadar Lemak ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Lemak",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Lemak ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldLemak,
                                    hintText: 'Kadar Lemak',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Kadar Protein ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis Salmonella",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Protein ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldProtein,
                                    hintText: 'Kadar Protein',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Kadar Karbohidrat ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Analisis S. Aureus",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Karbohidrat ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKarbohidrat,
                                    hintText: 'Kadar Karbohidrat',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Kadar Air ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Air",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Air ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldAir,
                                    hintText: 'Kadar Air',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Kadar Abu ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Abu",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Kadar Abu ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldAbu,
                                    hintText: 'Kadar Abu',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          // ------------------------------------ Button ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.theme,
                                      alignment: Alignment.center,
                                      duration: const Duration(milliseconds: 700),
                                      child: HomeAdminActivity()));
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Simpan',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
