import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kedaireka/ui/auth/login.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:page_transition/page_transition.dart';

class AddSuccess extends StatefulWidget {
  @override
  _AddSuccessState createState() => _AddSuccessState();
}

class _AddSuccessState extends State<AddSuccess> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        // backgroundColor: CustomColor.primaryLight,
        body: SafeArea(
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 86),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/success.png",
                        width: CustomSize.sizeWidth(context) / 1.1,
                        height: CustomSize.sizeWidth(context) / 1.1,
                      ),
                      CustomText.textTitle24(
                        text: "Berhasil",
                        maxLines: 1,
                        color: CustomColor.black,
                        MediaQuery: CustomSize.sizeWidth(context),
                      ),
                      CustomText.bodyMedium14(
                        text: "Data anda telah tercatat dalam sistem",
                        maxLines: 1,
                        color: CustomColor.primaryLight,
                        MediaQuery: CustomSize.sizeWidth(context),
                      ),
                      // ------------------------------------------------------------------------

                      SizedBox(
                        height: CustomSize.sizeHeight(context) / 32,
                      ),

                      // ------------------------------------ Button Masuk ------------------------------------
                      (isLoading != true)?GestureDetector(
                        onTap: (){
                          Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  type: PageTransitionType.theme,
                                  alignment: Alignment.center,
                                  duration: const Duration(milliseconds: 700),
                                  child: const HomeAdminActivity()));
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 14),
                          child: CustomButton.primaryButton(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context),
                              text: 'Kembali ke Halaman Utama',
                              MediaQuery: CustomSize.sizeWidth(context)
                          ),
                        ),
                      ):Padding(
                        padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 14),
                        child: CustomButton.primaryButtonLoading(
                            height: CustomSize.sizeHeight(context) / 12,
                            width: CustomSize.sizeWidth(context)
                        ),
                      ),
                      // ------------------------------------------------------------------------
                    ],
                  ),
                ),
              ),
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
