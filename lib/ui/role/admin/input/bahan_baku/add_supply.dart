import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/auth/regist.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/admin/input/bahan_baku/add_success.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class AddSupply extends StatefulWidget {
  const AddSupply({Key? key}) : super(key: key);

  @override
  _AddSupplyState createState() => _AddSupplyState();
}

class _AddSupplyState extends State<AddSupply> {

  final TextEditingController fieldWarna = TextEditingController(text: "");
  final TextEditingController fieldDiameter = TextEditingController(text: "");
  final TextEditingController fieldKadarNitrit = TextEditingController(text: "");
  final TextEditingController fieldBeratAwal = TextEditingController(text: "");
  final TextEditingController fieldKebersihan = TextEditingController(text: "");
  final TextEditingController fieldGrade = TextEditingController(text: "");

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 24),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Masuk ------------------------------------
                          Container(
                            // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                text: "Buat Suplai Baru",
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan data kualitas bahan baku",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "yang akan disuplai",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                // color: CustomColor.primaryLight2,
                                border: Border.all(color: CustomColor.primaryLight2)
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 20, vertical: CustomSize.sizeWidth(context) / 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // ------------------------------------ Warna ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Warna",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Warna ------------------------------------
                                  CustomTextField.textField(
                                      controller: fieldWarna,
                                      hintText: 'Warna Sarang burung walet',
                                      type: TextInputType.text,
                                      height: CustomSize.sizeHeight(context),
                                      width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Diameter ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Diameter",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Diameter ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldDiameter,
                                    hintText: 'Diameter Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Nitrit ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Kadar Nitrit",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Nitrit ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKadarNitrit,
                                    hintText: 'Kadar nitrit Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Berat Awal ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Berat Awal",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Berat Awal ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldBeratAwal,
                                    hintText: 'Berat awal Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Tingkat Kebersihan ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Tingkat Kebersihan",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Tingkat Kebersihan ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldKebersihan,
                                    hintText: 'Tingkat kebersihan Sarang burung walet',
                                    type: TextInputType.number,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Grade ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Grade",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Text Field Grade ------------------------------------
                                  CustomTextField.textField(
                                    controller: fieldGrade,
                                    hintText: 'Grade Sarang burung walet',
                                    type: TextInputType.text,
                                    height: CustomSize.sizeHeight(context),
                                    width: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                                  // ------------------------------------ Foto ------------------------------------
                                  CustomText.bodyMedium16(
                                    text: "   Foto",
                                    maxLines: 1,
                                    color: CustomColor.primaryLight,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  // ------------------------------------------------------------------------

                                  SizedBox(
                                    height: CustomSize.sizeHeight(context) * 0.005,
                                  ),

                                  // ------------------------------------ Foto Picker ------------------------------------
                                  Container(
                                    height: CustomSize.sizeWidth(context) / 2.5,
                                    width: CustomSize.sizeWidth(context) / 2.5,
                                    decoration: BoxDecoration(
                                      color: const Color(0xffF5F5F5),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Icon(FontAwesome.plus, color: CustomColor.primary, size: double.parse(((CustomSize.sizeWidth(context)*0.08).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.08).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.08).toString()),),
                                  )

                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          // ------------------------------------ Button Masuk ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.theme,
                                      alignment: Alignment.center,
                                      duration: const Duration(milliseconds: 700),
                                      child: AddSuccess()));
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Berikutnya',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 40,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
