import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kedaireka/ui/role/admin/profile/profile_activity.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_navbar.dart';
import 'package:kedaireka/utils/custom_package.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:page_transition/page_transition.dart';

class HomeAdminActivity extends StatefulWidget {
  const HomeAdminActivity({Key? key}) : super(key: key);

  @override
  _HomeAdminActivityState createState() => _HomeAdminActivityState();
}

class _HomeAdminActivityState extends State<HomeAdminActivity> {
  int indexPage = 0;

  void _changeSelectedNavBar(int index) {
    setState(() {
      indexPage = index;
    });
    print(indexPage);
    if (indexPage != 0) {
      if (indexPage == 0) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const HomeAdminActivity()));
      } else if (indexPage == 3) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const ProfileActivity()));
      }
      print(indexPage);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: CustomSize.sizeWidth(context),
              height: CustomSize.sizeHeight(context) / 3.75,
              decoration: BoxDecoration(
                  color: CustomColor.primary,
              ),
            ),
            SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 14, vertical: CustomSize.sizeWidth(context) / 24),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 88),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // ------------------------------------ Kedaireka ------------------------------------
                                Container(
                                    child: CustomText.textTitle24(
                                      text: "Kedaireka",
                                      color: Colors.white,
                                      MediaQuery: CustomSize.sizeWidth(context),
                                    )
                                ),
                                // ------------------------------------ Profile ------------------------------------
                                Container(
                                  width: CustomSize.sizeWidth(context) / 9,
                                  height: CustomSize.sizeWidth(context) / 9,
                                  decoration: BoxDecoration(
                                      color: CustomColor.secondary,
                                      shape: BoxShape.circle
                                  )
                                )
                              ],
                            ),
                          ),
                          SizedBox(height: CustomSize.sizeHeight(context) / 20,),
                          CustomPackage.menuHomeAdmin(
                              context: context,
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context)
                          ),
                          SizedBox(height: CustomSize.sizeHeight(context) / 42,),
                          Expanded(
                            child: ScrollConfiguration(
                              behavior: MyBehavior(),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                            child: CustomText.bodyMedium16(
                                              text: "Dalam Pengiriman",
                                              MediaQuery: CustomSize.sizeWidth(context),
                                            )
                                        ),
                                        CustomText.bodyMedium12(
                                          text: "Lebih Banyak",
                                          color: CustomColor.primary,
                                          MediaQuery: CustomSize.sizeWidth(context),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: CustomSize.sizeHeight(context) * 0.01,),
                                    CustomPackage.listBahanBaku(height: CustomSize.sizeHeight(context), width: CustomSize.sizeWidth(context), text: "PT Walet Abadi - Surabaya"),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  CustomPackage.hasilUji(height: CustomSize.sizeHeight(context),width: CustomSize.sizeWidth(context)),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: 0,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)!*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
          unselectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
          selectedItemColor: CustomColor.primaryLight,
          unselectedItemColor: CustomColor.primary,
          onTap: _changeSelectedNavBar,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Beranda',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.water),
              label: 'Kontrol Air',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.amp_stories_rounded),
              label: 'Kontrol Kemasan',
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesome.user),
              label: 'Profil',
            ),
          ],
        ),
      ),
    );
  }
}