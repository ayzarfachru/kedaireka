import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_navbar.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class ProfileActivity extends StatefulWidget {
  const ProfileActivity({Key? key}) : super(key: key);

  @override
  _ProfileActivityState createState() => _ProfileActivityState();
}

class _ProfileActivityState extends State<ProfileActivity> {
  final TextEditingController fieldNama = TextEditingController(text: "");
  final TextEditingController fieldEmail = TextEditingController(text: "");
  final TextEditingController fieldPassword = TextEditingController(text: "");

  int indexPage = 0;
  FocusNode? focusPassword;
  bool _obscureText = true;
  bool isLoading = false;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _changeSelectedNavBar(int index) {
    setState(() {
      indexPage = index;
    });
    if (indexPage != 3) {
      if (indexPage == 0) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const HomeAdminActivity()));
      } else if (indexPage == 3) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const ProfileActivity()));
      }
      print(indexPage);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Masuk ------------------------------------
                          Container(
                            alignment: Alignment.center,
                            height: CustomSize.sizeWidth(context) / 3.3,
                            width: CustomSize.sizeWidth(context) / 3.3,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: CustomColor.secondary,
                            ),
                            child: Icon(FontAwesome.camera, color: const Color(0xffF5F5F5), size: double.parse(((CustomSize.sizeWidth(context)*0.08).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.08).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.08).toString()),),
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Ubah",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          // ------------------------------------ Nama ------------------------------------
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText.bodyMedium16(
                                text: "   Nama",
                                maxLines: 1,
                                color: CustomColor.primaryLight,
                                MediaQuery: CustomSize.sizeWidth(context),
                              ),
                              // ------------------------------------------------------------------------

                              SizedBox(
                                height: CustomSize.sizeHeight(context) * 0.005,
                              ),

                              // ------------------------------------ Text Field Nama ------------------------------------
                              CustomTextField.textField(
                                  controller: fieldNama,
                                  height: CustomSize.sizeHeight(context),
                                  width: CustomSize.sizeWidth(context)
                              ),
                              // ------------------------------------------------------------------------

                              SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                              // ------------------------------------ Email ------------------------------------
                              CustomText.bodyMedium16(
                                text: "   Email",
                                maxLines: 1,
                                color: CustomColor.primaryLight,
                                MediaQuery: CustomSize.sizeWidth(context),
                              ),
                              // ------------------------------------------------------------------------

                              SizedBox(
                                height: CustomSize.sizeHeight(context) * 0.005,
                              ),

                              // ------------------------------------ Text Field Email ------------------------------------
                              CustomTextField.textField(
                                  controller: fieldEmail,
                                  type: TextInputType.emailAddress,
                                  height: CustomSize.sizeHeight(context),
                                  width: CustomSize.sizeWidth(context)
                              ),
                              // ------------------------------------------------------------------------

                              SizedBox(height: CustomSize.sizeHeight(context) / 48,),


                              // ------------------------------------ Password ------------------------------------
                              CustomText.bodyMedium16(
                                text: "   Password",
                                maxLines: 1,
                                color: CustomColor.primaryLight,
                                MediaQuery: CustomSize.sizeWidth(context),
                              ),
                              // ------------------------------------------------------------------------

                              SizedBox(
                                height: CustomSize.sizeHeight(context) * 0.005,
                              ),

                              // ------------------------------------ Text Field Password ------------------------------------
                              CustomTextField.textFieldPassword(
                                  height: CustomSize.sizeHeight(context),
                                  width: CustomSize.sizeWidth(context),
                                  controller: fieldPassword,
                                  focusNode: focusPassword,
                                  bool: _obscureText,
                                  gestureDetector: GestureDetector(
                                    onTap: _toggle,
                                    child: Icon(
                                        _obscureText
                                            ? MaterialCommunityIcons.eye
                                            : MaterialCommunityIcons.eye_off,
                                        color: CustomColor.primary),
                                  )
                              ),
                              // ------------------------------------------------------------------------
                            ],
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 16,
                          ),

                          // ------------------------------------ Button Daftar ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.theme,
                                      alignment: Alignment.center,
                                      duration: const Duration(milliseconds: 700),
                                      child: HomeAdminActivity()));
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Simpan',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeAdminActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: 3,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)!*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
          unselectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
          selectedItemColor: CustomColor.primaryLight,
          unselectedItemColor: CustomColor.primary,
          onTap: _changeSelectedNavBar,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Beranda',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.water),
              label: 'Kontrol Air',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.amp_stories_rounded),
              label: 'Kontrol Kemasan',
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesome.user),
              label: 'Profil',
            ),
          ],
        ),
      ),
    );
  }
}
