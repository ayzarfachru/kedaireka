import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_navbar.dart';
import 'package:kedaireka/utils/custom_package.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class DetailEsnActivity extends StatefulWidget {
  const DetailEsnActivity({Key? key}) : super(key: key);

  @override
  _DetailEsnActivityState createState() => _DetailEsnActivityState();
}

class _DetailEsnActivityState extends State<DetailEsnActivity> {
  final TextEditingController fieldNama = TextEditingController(text: "");
  final TextEditingController fieldEmail = TextEditingController(text: "");
  final TextEditingController fieldPassword = TextEditingController(text: "");

  int indexPage = 0;
  FocusNode? focusPassword;
  bool _obscureText = true;
  bool isLoading = false;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _changeSelectedNavBar(int index) {
    setState(() {
      indexPage = index;
    });
    if (indexPage != 3) {
      if (indexPage == 0) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const HomeAdminActivity()));
      } else if (indexPage == 3) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const DetailEsnActivity()));
      }
      print(indexPage);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Masuk ------------------------------------
                          CustomPackage.imageDetailHomeUser(height: CustomSize.sizeHeight(context), width: CustomSize.sizeWidth(context)),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 32,
                          ),

                          Container(
                            alignment: Alignment.centerLeft,
                            child: CustomText.textTitle24(
                              text: "Apa itu ESN ?",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          Container(
                            alignment: Alignment.centerLeft,
                            child: CustomText.bodyRegular15(
                              text: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                              maxLines: 200,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 48,
                          ),

                          CustomPackage.imageDetailSquare(image: 2, height: CustomSize.sizeHeight(context), width: CustomSize.sizeWidth(context)),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
