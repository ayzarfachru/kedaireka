import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kedaireka/ui/auth/login.dart';
import 'package:kedaireka/ui/role/admin/profile/profile_activity.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_navbar.dart';
import 'package:kedaireka/utils/custom_package.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:page_transition/page_transition.dart';

class HomeUserActivity extends StatefulWidget {
  const HomeUserActivity({Key? key}) : super(key: key);

  @override
  _HomeUserActivityState createState() => _HomeUserActivityState();
}

class _HomeUserActivityState extends State<HomeUserActivity> {
  int indexPage = 0;

  void _changeSelectedNavBar(int index) {
    setState(() {
      indexPage = index;
    });
    print(indexPage);
    if (indexPage != 0) {
      if (indexPage == 0) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const HomeUserActivity()));
      } else if (indexPage == 3) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const ProfileActivity()));
      }
      print(indexPage);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: CustomSize.sizeWidth(context),
              height: CustomSize.sizeHeight(context) / 3.75,
              decoration: BoxDecoration(
                color: CustomColor.primary,
              ),
            ),
            SafeArea(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 14, vertical: CustomSize.sizeWidth(context) / 24),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 88),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // ------------------------------------ Kedaireka ------------------------------------
                          Container(
                              child: CustomText.textTitle24(
                                text: "Selamat datang di\nKedaireka👋",
                                color: Colors.white,
                                maxLines: 2,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),
                          // ------------------------------------ Login ------------------------------------
                          GestureDetector(
                              onTap: (){
                                Navigator.pushReplacement(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.theme,
                                        alignment: Alignment.center,
                                        duration: const Duration(milliseconds: 700),
                                        child: const LoginActivity()));
                              },
                              child: Icon(Icons.login_rounded, color: Colors.white, size: double.parse(((CustomSize.sizeWidth(context)*0.075).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.075).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.075).toString()))
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: CustomSize.sizeHeight(context) / 28,),
                    CustomPackage.menuHomeUser(
                        context: context,
                        height: CustomSize.sizeHeight(context),
                        width: CustomSize.sizeWidth(context),
                        label: "Profile Perusahaan Kami",
                        text: "CV Dwi Anugerah Surabaya",
                        image: true
                    ),
                    SizedBox(height: CustomSize.sizeHeight(context) / 42,),
                    Expanded(
                      child: ScrollConfiguration(
                        behavior: MyBehavior(),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                  child: CustomText.textTitle20(
                                    text: "Tentang ESN",
                                    color: CustomColor.primary,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  )
                              ),
                              SizedBox(height: CustomSize.sizeHeight(context) / 42,),
                              CustomPackage.menuHomeUser(
                                  context: context,
                                  height: CustomSize.sizeHeight(context),
                                  width: CustomSize.sizeWidth(context),
                                  label: "Apa itu ESN ?",
                                  text: "Pengantar tentang Edible Swallow Nest",
                                  page: '2',
                                  image: false
                              ),
                              SizedBox(height: CustomSize.sizeHeight(context) * 0.02,),
                              CustomPackage.menuHomeUser(
                                  context: context,
                                  height: CustomSize.sizeHeight(context),
                                  width: CustomSize.sizeWidth(context),
                                  label: "Adulteran ESN",
                                  text: "Sarang burung walet palsu yang beredar di pasaran",
                                  image: false
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}