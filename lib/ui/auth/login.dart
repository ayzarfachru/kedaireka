import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/auth/regist.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/user/home/home_user_activity.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/url/url.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';

class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class LoginActivity extends StatefulWidget {
  const LoginActivity({Key? key}) : super(key: key);

  @override
  _LoginActivityState createState() => _LoginActivityState();
}

class _LoginActivityState extends State<LoginActivity> {

  final TextEditingController fieldEmail = TextEditingController(text: "");
  final TextEditingController fieldPassword = TextEditingController(text: "");

  FocusNode? focusPassword;
  bool _obscureText = true;
  bool isLoading = false;
  String warning = '';

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  Future _login(String email, String password) async {
    if(email != "" && password != ""){
      setState(() {
        isLoading = true;
      });
      var apiResult = await http.post(Uri.parse(Links.mainUrl + '/auth/login'), body: {'email': email, 'password': password, 'auth_by_google ': 'false'});
      print('apiResult.body login');
      print(apiResult.body);
      var data = json.decode(apiResult.body);

    }else{
      warning = "Datamu kurang lengkap nih";
    }
    setState(() {
      isLoading = false;
    });
  }

  final GoogleSignIn _googleSignIn = GoogleSignIn(
    clientId: "174275429662-i244o4nrdiie45ke3tr6o8fau6nrdcd3.apps.googleusercontent.com",
    scopes: [
      'email',
      'https://www.googleapis.com/auth/userinfo.profile',
      // 'https://www.googleapis.com/auth/user.birthday.read',
      // 'https://www.googleapis.com/auth/user.gender.read',
      // 'https://www.googleapis.com/auth/user.phonenumbers.read'
    ],
  );

  Future<void> _loginByGoogle() async {
    await _googleSignIn.signOut();
    await _googleSignIn.signIn().then((value) async{
      var apiResult = await http.post(Uri.parse(Links.mainUrl + '/auth/login'),
          body: {'email': value?.email, 'password': '', 'auth_by_google': 'true'});
      print('_loginByGoogle');
      print(apiResult.body);
      var data = json.decode(apiResult.body);
      if (apiResult.statusCode == 200) {

        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setInt("id", int.parse(data['user']['id'].toString()));
        pref.setString("name", data['user']['name'].toString());
        pref.setString("email", data['user']['email'].toString());
        pref.setString("img", data['user']['img'].toString());
        pref.setString("gender", data['user']['gender'].toString());
        pref.setString("tgl", data['user']['ttl'].toString());
        pref.setString("notelp", data['user']['phone_number'].toString());
        pref.setString("token", data['access_token'].toString());
        // pref.setString("timeLog", DateTime.now().toString().toString().split('-')[2].replaceAll('.', '').replaceAll(':', '').replaceAll('T', ''));

        print('Time ${DateTime.now()}');
        print('Time ${DateTime.now().toString().toString().split('-')[2].replaceAll('.', '').replaceAll(':', '').replaceAll('T', '')}');

        print('${data['user']['id']} telp');

        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const LoginActivity()));
        // if (mounted == false) {
        //   Fluttertoast.showToast(
        //       msg: 'Tekan sekali lagi untuk login!');
        //   // Future.delayed(Duration(seconds: 5), () async {
        //   //   print('ini ' + mounted.toString());
        //   //   _handleSignIn();
        //   // });
        // } else {
        //   Fluttertoast.showToast(
        //       msg: 'Login berhasil!');
        //   showDialog(
        //       context: context,
        //       builder: (context) {
        //         return AlertDialog(
        //           contentPadding: EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 5),
        //           shape: RoundedRectangleBorder(
        //               borderRadius: BorderRadius.all(Radius.circular(10))
        //           ),
        //           title: Center(child: Text('Terms Conditions', style: TextStyle(color: CustomColor.redBtn, fontSize: double.parse(((MediaQuery.of(context).size.width*0.06).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.06)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.06)).toString())))),
        //           content: Container(
        //             height: CustomSize.sizeHeight(context) / 2,
        //             width: CustomSize.sizeWidth(context) / 1.5,
        //             child: ListView(
        //               physics: AlwaysScrollableScrollPhysics(),
        //               padding: EdgeInsets.zero,
        //               shrinkWrap: true,
        //               children: [
        //                 Text('Pendahuluan', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.04)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.04)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('PT Imaji Cipta (mempunyai produk yang disebut “Indonesia Resto Guide”) ialah suatu perseroan terbatas yang salah satu jenis usahanya berkecimpung pada bidang portal penjualan di bidang kuliner. Indonesia Resto Guide. PT Imaji Cipta dalam hal ini menyediakan Platform penjualan elektronik (e-commerce) di mana Pengguna dapat melakukan transaksi jual-beli, menggunakan berbagai fitur serta layanan yang tersedia. Setiap pihak yang berada pada wilayah Negara Kesatuan Republik Indonesia bisa mengakses Platform Indonesia Resto Guide untuk membuka lapangan penjualan di bidang kuliner, menggunakan layanan, atau hanya sekedar mengakses / mengunjungi. \n\nSyarat & ketentuan yang telah ditetapkan untuk mengatur pemakaian jasa yang ditawarkan oleh PT. Imaji Cipta terkait penggunaan perangkat lunak Indonesia Resto Guide. Pengguna disarankan membaca dengan seksama karena dapat berdampak pada hak dan kewajiban Pengguna di bawah aturan. dengan mendaftar akun Indonesia Resto Guide dan /atau memakai Platform Indonesia Resto Guide, maka Pengguna dianggap sudah membaca, mengerti, tahu serta menyetujui seluruh isi pada aturan Penggunaan. Jika pengguna tidak menyetujui salah satu, pesebagian, atau semua isi syarat & ketentuan, maka pengguna tidak diperkenankan memakai layanan Indonesia Resto Guide.', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.035)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.035)).toString()), fontWeight: FontWeight.w400), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Definisi', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.04)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.04)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Dalam Aturan Penggunaan istilah-istilah di bawah ini mempunyai arti sebagai berikut:', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('1.)	PT. Imaji Cipta (Indonesia Resto Guide) adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa aplikasi Indonesia Resto Guide, yakni aplikasi pencarian lapak dan Kuliner yang dijual oleh penjual terdaftar. Yang selanjutnya disebut Indonesia Resto Guide. \n\n2.)	Akun adalah data tentang Pengguna, minimum terdiri dari nama, password, nomor telepon, dan email yang wajib diisi oleh Pengguna Terdaftar. \n\n3.)	Platform Indonesia Resto Guide adalah situs resmi indonesiarestoguide.com dan seluruh website resmi beserta aplikasi resmi Indonesia Resto Guide (berbasis Android dan iOS) yang dapat diakses melalui perangkat komputer dan/atau perangkat seluler Pengguna. \n\n4.)	Pembeli adalah Pengguna terdaftar yang melakukan permintaan atas Makanan atau minuman yang dijual oleh Penjual di Aplikasi Indonesia Resto Guide. \n\n5.)	Penjual adalah Pengguna terdaftar yang melakukan kegiatan buka toko dan/atau melakukan penawaran atas suatu Makanan dan minuman kepada para Pengguna dan /atau Pembeli. \n\n6.)	Layanan adalah secara kolektif: (i) Platform Indonesia Resto Guide; (ii) Konten, fitur, layanan, dan fungsi apa pun yang tersedia di atau melalui Platform oleh atau atas nama Indonesia Resto Guide, termasuk Layanan Partner; dan pemberitahuan email, tombol, widget, dan iklan.', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.035)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.035)).toString()), fontWeight: FontWeight.w400), textAlign: TextAlign.justify, ),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Pengguna, Penjual, Akun, Password & Keamanan', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.04)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.04)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('1.)	Pengguna wajib berusia minimal 18 tahun (kecuali ditentukan lain oleh peraturan perundang-undangan yang berlaku di Indonesia). Pengguna yang belum genap berusia 18 tahun wajib memperoleh persetujuan dari orang tua atau wali untuk menggunakan dan /atau mengakses layanan di Platform Indonesia Resto Guide dan bertanggung jawab atas segala biaya yang timbul terkait penggunaan layanan di Platform Indonesia Resto Guide. \n\n2.)	Pengguna harus memahami bahwa 1 (satu) nomor telepon hanya dapat digunakan untuk mendaftar 1 (satu) akun Pengguna Indonesia Resto Guide, kecuali bagi Pengguna yang telah memiliki beberapa akun dengan 1 (satu) nomor telepon sebelumnya \n\n3.)	Pengguna yang telah mendaftar berhak bertindak sebagai: Pembeli dan Penjual. \n\n4.)	Penjual diwajibkan membayar biaya pembukaan toko. Penjual berhak melakukan pengaturan terhadap barang yang akan diperdagangkan di lapak pribadi Penjual. \n\n5.)	Indonesia Resto Guide memiliki hak untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran Syarat & ketentuan sesuai dengan hukum yang berlaku, yakni tindakan berupa penghapusan Barang, penutupan toko, suspensi akun, sampai penghapusan akun pengguna. \n\n6.)	Pengguna menyetujui untuk tidak menggunakan dan/atau mengakses sistem Indonesia Resto Guide secara langsung atau tidak langsung, baik keseluruhan atau sebagian dengan virus, perangkat lunak, atau teknologi lainnya yang dapat mengakibatkan melemahkan, merusak, mengganggu dan menghambat, membatasi, mengambil alih fungsionalitas serta integritas dari sistem perangkat lunak atau perangkat keras, jaringan, dan/atau data pada Aplikasi Indonesia Resto Guide. \n\n7.)	Pengguna wajib mengetahui bahwa detail informasi berupa data diri nama, alamat usaha, nomor telepon akun milik Pengguna akan diterima oleh pihak Penjual dalam kemudahan bertransaksi dan berfungsi sebagai database penjual sendiri \n\n8.)	Penjual harus mengetahui bahwa detail informasi milik Pengguna adalah rahasia, dan karenanya Penjual tidak akan mengungkapkan detail informasi akun Pengguna kepada Pihak Ketiga mana pun kecuali untuk kegiatan jual beli dalam aplikasi Indonesia Resto Guide. \n\n9.)	Penjual setuju untuk menanggung setiap risiko terkait pengungkapan informasi Akun Pengguna kepada Pihak Ketiga mana pun dan bertanggung jawab penuh atas setiap konsekuensi yang berkaitan dengan hal tersebut. \n\n10.)	Pengguna dilarang menggunakan Platform Indonesia Resto Guide untuk melanggar peraturan yang ditetapkan oleh hukum di Indonesia maupun di negara lainnya. \n\n11.)	Pengguna dilarang mendistribusikan virus atau teknologi lainnya yang dapat membahayakan aplikasi Indonesia Resto Guide, kepentingan dan/atau properti dari Pengguna lain, maupun instansi Pemerintahan. \n\n12.)	Pengguna dilarang menggunakan Platform Indonesia Resto Guide untuk tujuan komersial dan melakukan transfer/menjual akun Pengguna ke Pengguna lain atau ke pihak lain dengan tujuan apapun. \n\n13.)	Pengguna wajib menghargai hak-hak Pengguna lainnya dengan tidak memberikan informasi pribadi ke pihak lain tanpa izin pihak yang bersangkutan. \n\n14.)	Pengguna wajib membaca, memahami serta mengikuti semua ketentuan yang diatur dalam Aturan Penggunaan ini.', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.035)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.035)).toString()), fontWeight: FontWeight.w400), textAlign: TextAlign.justify, ),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Ketentuan Lain', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.04)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.04)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('1.)	Apabila pengguna mempergunakan fitur/layanan yang tersedia dalam Website/Aplikasi Indonesia Resto Guide, maka Pengguna dengan ini menyatakan telah memahami dan menyetujui segala syarat dan ketentuan yang diatur khusus sehubungan dengan fitur/layanan yang digunakan. \n\n2.)	Segala hal yang belum dan/atau tidak diatur dalam syarat dan ketentuan khusus dalam fitur tersebut maka akan sepenuhnya merujuk pada syarat dan ketentuan Indonesia Resto Guide secara umum. \n\n3.)	Dengan menyetujui Syarat dan Ketentuan, maka Pengguna telah dianggap paham dan mengikuti Kebijakan Privasi Indonesia Resto Guide.', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.035)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.035)).toString()), fontWeight: FontWeight.w400), textAlign: TextAlign.justify, ),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Pembaruan & Perubahan Aturan Penggunaan', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.04)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.04)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.03).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.03)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.03)).toString()), fontWeight: FontWeight.w500), textAlign: TextAlign.start),
        //                 Text('Indonesia Resto Guide memiliki hak untuk melakukan pembaruan dan/atau perubahan Aturan Penggunaan dari waktu ke waktu jika diperlukan demi keamanan dan kenyamanan Pengguna di Platform Indonesia Resto Guide. Pengguna harus setuju untuk membaca secara saksama dan memeriksa Aturan Penggunaan ini dari waktu ke waktu untuk mengetahui pembaruan dan/atau perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Indonesia Resto Guide, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & Ketentuan kami.', style: TextStyle(fontSize: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?((MediaQuery.of(context).size.width*0.035)).toString().split('.')[0]:((MediaQuery.of(context).size.width*0.035)).toString()), fontWeight: FontWeight.w400), textAlign: TextAlign.start, ),
        //               ],
        //             ),
        //           ),
        //           actions: <Widget>[
        //             Center(
        //               child: Row(
        //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
        //                 children: [
        //                   // FlatButton(
        //                   //   // minWidth: CustomSize.sizeWidth(context),
        //                   //   color: CustomColor.redBtn,
        //                   //   textColor: Colors.white,
        //                   //   shape: RoundedRectangleBorder(
        //                   //       borderRadius: BorderRadius.all(Radius.circular(10))
        //                   //   ),
        //                   //   child: Text('Batal'),
        //                   //   onPressed: () async{
        //                   //     setState(() {
        //                   //       // codeDialog = valueText;
        //                   //       Navigator.pop(context);
        //                   //     });
        //                   //   },
        //                   // ),
        //                   FlatButton(
        //                     color: CustomColor.primaryLight,
        //                     textColor: Colors.white,
        //                     shape: RoundedRectangleBorder(
        //                         borderRadius: BorderRadius.all(Radius.circular(10))
        //                     ),
        //                     child: Text('Setuju'),
        //                     onPressed: () async{
        //                       Navigator.pop(context);
        //                       Navigator.pushReplacement(
        //                           context,
        //                           PageTransition(
        //                               type: PageTransitionType.rightToLeft,
        //                               child: HomeActivity()));
        //                       terms = true;
        //                       setState(() {});
        //                     },
        //                   ),
        //                 ],
        //               ),
        //             ),
        //
        //           ],
        //         );
        //       });
        // }
      }else{

      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 18),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Masuk ------------------------------------
                          Container(
                              // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                  text: "Masuk",
                                  MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan username dan password",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Anda untuk masuk ke aplikasi",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          // ------------------------------------ Email ------------------------------------
                          CustomText.bodyMedium16(
                            text: "   Email",
                            maxLines: 1,
                            color: CustomColor.primaryLight,
                            MediaQuery: CustomSize.sizeWidth(context),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.005,
                          ),

                          // ------------------------------------ Text Field Email ------------------------------------
                          CustomTextField.textField(
                              controller: fieldEmail,
                              type: TextInputType.emailAddress,
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                          // ------------------------------------ Password ------------------------------------
                          CustomText.bodyMedium16(
                            text: "   Password",
                            maxLines: 1,
                            color: CustomColor.primaryLight,
                            MediaQuery: CustomSize.sizeWidth(context),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.005,
                          ),

                          // ------------------------------------ Text Field Password ------------------------------------
                          CustomTextField.textFieldPassword(
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context),
                              controller: fieldPassword,
                              focusNode: focusPassword,
                              bool: _obscureText,
                              gestureDetector: GestureDetector(
                                onTap: _toggle,
                                child: Icon(
                                    _obscureText
                                        ? MaterialCommunityIcons.eye
                                        : MaterialCommunityIcons.eye_off,
                                    color: CustomColor.primary),
                              )
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context)*0.01,
                          ),

                          // ------------------------------------ Lupa Password ------------------------------------
                          Container(
                            alignment: Alignment.centerRight,
                            child: CustomText.textTitle12(
                              text: "Lupa password?",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            )
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 16,
                          ),

                          // ------------------------------------ Button Masuk ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              _login(fieldEmail.text, fieldPassword.text);
                              // if (fieldEmail.text == '' || fieldPassword.text == '') {
                              //   // Fluttertoast.showToast(msg: "Please fill in your email and password");
                              // } else {
                              //   // if (terms == false) {
                              //   //   Fluttertoast.showToast(msg: "Baca lalu setujui Terms Conditions untuk melanjutkan.");
                              //   // } else {
                              //   //   idPlayer().whenComplete(() {
                              //   //     terms = true;
                              //   //     setState(() {});
                              //   //     _login(_loginTextEmail.text, _loginTextPassword.text);
                              //   //   });
                              //   // }
                              //   FocusScope.of(context).requestFocus(FocusNode());
                              //   setState((){});
                              // }
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Masuk',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          (warning != '')?SizedBox(
                            height: CustomSize.sizeHeight(context) / 58,
                          ):Container(),
                          (warning != '')?Container(
                              alignment: Alignment.center,
                              child: CustomText.textTitle12(
                                text: warning,
                                maxLines: 1,
                                color: CustomColor.redBtn,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ):Container(),
                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.002,
                          ),
                          const Divider(),
                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.002,
                          ),

                          // ------------------------------------ Button Masuk dengan Google ------------------------------------
                          GestureDetector(
                            onTap: (){
                              _loginByGoogle();
                              FocusScope.of(context).requestFocus(FocusNode());
                              setState((){});
                              // _handleSignIn();
                            },
                            child: CustomButton.googleButton(
                                height: CustomSize.sizeHeight(context), width: CustomSize.sizeWidth(context)
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(height: CustomSize.sizeHeight(context) * 0.01,),
                          // (message != '')?Center(child: Padding(
                          //   padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 32),
                          //   child: CustomText.bodyMedium16(text: (message != 'email existed')?message:'email telah digunakan', maxLines: 10, color: CustomColor.redBtn),
                          // )):Container(),
                          SizedBox(height: CustomSize.sizeHeight(context) * 0.01,),

                          // ------------------------------------ Text Belum memiliki akun ------------------------------------
                          Center(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pushReplacement(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.theme,
                                        alignment: Alignment.center,
                                        duration: const Duration(milliseconds: 700),
                                        child: const RegistActivity()));
                              },
                              child: Column(
                                children: [
                                  CustomText.bodyMedium16(
                                    text: "Belum memiliki akun?",
                                    maxLines: 2,
                                    color: Colors.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  CustomText.bodyMedium16(
                                    text: "Daftar disini.",
                                    maxLines: 2,
                                    color: Colors.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Stack(
                alignment: Alignment.centerLeft,
                children: [
                  Container(
                    height: CustomSize.sizeHeight(context) / 14,
                    color: Colors.white,
                    alignment: Alignment.center,
                    child: CustomText.textTitle16(
                      text: "Kedaireka",
                      maxLines: 1,
                      color: CustomColor.black,
                      MediaQuery: CustomSize.sizeWidth(context),
                      // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 38),
                      child: GestureDetector(
                          onTap: (){
                            Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.theme,
                                    alignment: Alignment.center,
                                    duration: const Duration(milliseconds: 700),
                                    child: const HomeUserActivity()));
                          },
                          child: Icon(Icons.arrow_back, color: Colors.black, size: double.parse(((CustomSize.sizeWidth(context)*0.0675).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.0675).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.0675).toString()))
                      )
                  ),
                ],
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
