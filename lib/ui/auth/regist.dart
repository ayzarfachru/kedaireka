import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/url/url.dart';
import 'package:kedaireka/utils/custom_button.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:kedaireka/utils/custom_text_field.dart';
import 'package:page_transition/page_transition.dart';
import 'package:http/http.dart' as http;

import 'login.dart';


class CustomScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class RegistActivity extends StatefulWidget {
  const RegistActivity({Key? key}) : super(key: key);

  @override
  _RegistActivityState createState() => _RegistActivityState();
}

class _RegistActivityState extends State<RegistActivity> {

  TextEditingController fieldNama = TextEditingController(text: "");
  TextEditingController fieldEmail = TextEditingController(text: "");
  TextEditingController fieldPassword = TextEditingController(text: "");

  FocusNode? focusPassword;
  bool _obscureText = true;
  bool isLoading = false;
  String warning = '';

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  Future _regist(String email, String name, String password) async {
    if(email != "" && name != "" && password != ""){
      setState(() {
        isLoading = true;
      });
      var apiResult = await http.post(Uri.parse('${Links.mainUrl}/auth/register'), body: {'email': email, 'name': name, 'password': password});
      print('apiResult.body login');
      print(apiResult.body);
      // var data = json.decode(apiResult.body);

      if (apiResult.statusCode == 200) {
        warning = 'Data anda berhasil terdaftar';
        fieldNama = TextEditingController(text: "");
        fieldEmail = TextEditingController(text: "");
        fieldPassword = TextEditingController(text: "");
        isLoading = false;
      } else {
        warning = 'Data anda gagal terdaftar!';
        isLoading = false;
      }

    }else{
      warning = "Datamu kurang lengkap nih";
      isLoading = false;
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Center(
                child: ScrollConfiguration(
                  behavior: CustomScroll(),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 18),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),

                          // ------------------------------------ Masuk ------------------------------------
                          Container(
                            // alignment: Alignment.center,
                              child: CustomText.textTitle24(
                                text: "Daftar",
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.01,
                          ),

                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "Masukkan data diri Anda",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          Container(
                            // alignment: Alignment.center,
                            child: CustomText.bodyRegular14(
                              text: "untuk membuat akun baru",
                              maxLines: 1,
                              color: CustomColor.primaryLight,
                              MediaQuery: CustomSize.sizeWidth(context),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 24,
                          ),

                          // ------------------------------------ Nama ------------------------------------
                          CustomText.bodyMedium16(
                            text: "   Nama",
                            maxLines: 1,
                            color: CustomColor.primaryLight,
                            MediaQuery: CustomSize.sizeWidth(context),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.005,
                          ),

                          // ------------------------------------ Text Field Nama ------------------------------------
                          CustomTextField.textField(
                              controller: fieldNama,
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(height: CustomSize.sizeHeight(context) / 48,),

                          // ------------------------------------ Email ------------------------------------
                          CustomText.bodyMedium16(
                            text: "   Email",
                            maxLines: 1,
                            color: CustomColor.primaryLight,
                            MediaQuery: CustomSize.sizeWidth(context),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.005,
                          ),

                          // ------------------------------------ Text Field Email ------------------------------------
                          CustomTextField.textField(
                              controller: fieldEmail,
                              type: TextInputType.emailAddress,
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(height: CustomSize.sizeHeight(context) / 48,),


                          // ------------------------------------ Password ------------------------------------
                          CustomText.bodyMedium16(
                            text: "   Password",
                            maxLines: 1,
                            color: CustomColor.primaryLight,
                            MediaQuery: CustomSize.sizeWidth(context),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) * 0.005,
                          ),

                          // ------------------------------------ Text Field Password ------------------------------------
                          CustomTextField.textFieldPassword(
                              height: CustomSize.sizeHeight(context),
                              width: CustomSize.sizeWidth(context),
                              controller: fieldPassword,
                              focusNode: focusPassword,
                              bool: _obscureText,
                              gestureDetector: GestureDetector(
                                onTap: _toggle,
                                child: Icon(
                                    _obscureText
                                        ? MaterialCommunityIcons.eye
                                        : MaterialCommunityIcons.eye_off,
                                    color: CustomColor.primary),
                              )
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context)*0.01,
                          ),

                          // ------------------------------------ Lupa Password ------------------------------------
                          Container(
                              alignment: Alignment.centerRight,
                              child: CustomText.textTitle12(
                                text: "Lupa password?",
                                maxLines: 1,
                                color: CustomColor.primaryLight,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 16,
                          ),

                          // ------------------------------------ Button Daftar ------------------------------------
                          (isLoading != true)?GestureDetector(
                            onTap: (){
                              _regist(fieldEmail.text, fieldNama.text, fieldPassword.text);
                              FocusScope.of(context).requestFocus(FocusNode());
                              setState((){});
                            },
                            child: CustomButton.primaryButton(
                                height: CustomSize.sizeHeight(context) / 12,
                                width: CustomSize.sizeWidth(context),
                                text: 'Daftar',
                                MediaQuery: CustomSize.sizeWidth(context)
                            ),
                          ):CustomButton.primaryButtonLoading(
                              height: CustomSize.sizeHeight(context) / 12,
                              width: CustomSize.sizeWidth(context)
                          ),
                          // ------------------------------------------------------------------------

                          (warning != '')?SizedBox(
                            height: CustomSize.sizeHeight(context) / 58,
                          ):Container(),
                          (warning != '')?Container(
                              alignment: Alignment.center,
                              child: CustomText.textTitle12(
                                text: warning,
                                maxLines: 1,
                                color: (warning.contains('gagal') != true)?CustomColor.accent:CustomColor.redBtn,
                                MediaQuery: CustomSize.sizeWidth(context),
                              )
                          ):Container(),
                          SizedBox(height: CustomSize.sizeHeight(context) * 0.01,),
                          // (message != '')?Center(child: Padding(
                          //   padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 32),
                          //   child: CustomText.bodyMedium16(text: (message != 'email existed')?message:'email telah digunakan', maxLines: 10, color: CustomColor.redBtn),
                          // )):Container(),
                          SizedBox(height: CustomSize.sizeHeight(context) * 0.01,),

                          // ------------------------------------ Text Sudah memiliki akun ------------------------------------
                          Center(
                            child: GestureDetector(
                              onTap: (){
                                Navigator.pushReplacement(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.theme,
                                        alignment: Alignment.center,
                                        duration: const Duration(milliseconds: 700),
                                        child: const LoginActivity()));
                              },
                              child: Column(
                                children: [
                                  CustomText.bodyMedium16(
                                    text: "Sudah memiliki akun?",
                                    maxLines: 2,
                                    color: Colors.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                  CustomText.bodyMedium16(
                                    text: "Klik disini untuk Masuk.",
                                    maxLines: 2,
                                    color: Colors.black,
                                    MediaQuery: CustomSize.sizeWidth(context),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // ------------------------------------------------------------------------

                          SizedBox(
                            height: CustomSize.sizeHeight(context) / 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

              // ------------------------------------ Kedaireka Text ------------------------------------
              Container(
                height: CustomSize.sizeHeight(context) / 14,
                color: Colors.white,
                alignment: Alignment.center,
                child: CustomText.textTitle16(
                  text: "Kedaireka",
                  maxLines: 1,
                  color: CustomColor.black,
                  MediaQuery: CustomSize.sizeWidth(context),
                  // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
                ),
              ),
              // ------------------------------------------------------------------------

            ],
          ),
        ),
      ),
    );
  }
}
