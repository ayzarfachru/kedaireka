import 'package:flutter/material.dart';
import 'package:kedaireka/ui/auth/login.dart';
import 'package:kedaireka/ui/role/user/home/home_user_activity.dart';
import 'package:kedaireka/utils/custom_size.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  Future<bool> _checkForSession() async {
    await Future.delayed(Duration(milliseconds: 2000), () {});

    return true;
  }

  @override
  void initState() {
    _checkForSession().then((status) {
      if (status) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.theme,
                alignment: Alignment.center,
                duration: const Duration(milliseconds: 700),
                child: const HomeUserActivity()));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: Scaffold(
        // backgroundColor: CustomColor.primaryLight,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: CustomSize.sizeWidth(context) / 86),
          child: Center(
            child: Stack(
              alignment: Alignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.theme,
                            alignment: Alignment.center,
                            duration: const Duration(milliseconds: 700),
                            child: const LoginActivity()));
                  },
                  child: Image.asset(
                    "assets/splash_icon.png",
                    width: CustomSize.sizeWidth(context) / 1.1,
                    height: CustomSize.sizeWidth(context) / 1.1,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(
                            context,
                            PageTransition(
                                type: PageTransitionType.theme,
                                alignment: Alignment.center,
                                duration: const Duration(milliseconds: 700),
                                child: const HomeUserActivity()));
                      },
                      child: Image.asset(
                        "assets/kedaireka_logo.png",
                        width: CustomSize.sizeWidth(context) / 1.75,
                        height: CustomSize.sizeWidth(context) / 1.75,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
