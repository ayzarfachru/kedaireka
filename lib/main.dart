import 'package:flutter/material.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // title: 'Flutter Demo',
      theme: ThemeData(
        // primaryColor: CustomColor.primary,
        primaryColorBrightness: Brightness.light,
        scaffoldBackgroundColor: Colors.white,
        // accentColor: CustomColor.primary,
        accentColorBrightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        // appBarTheme: AppBarTheme(
        //     color: CustomColor.background,
        //     centerTitle: true,
        //     elevation: 0
        // ),
      ),
      // home: (isLogin)?new SplashScreen():new WelcomeScreen(),
      home: SplashScreen(),
    );
  }
}

