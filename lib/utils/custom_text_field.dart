import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kedaireka/utils/custom_colors.dart';

class CustomTextField{

  static Widget textField({TextEditingController? controller, String? hintText, double? height, double? width, TextInputType? type}){
    return Container(
      height: height! / 14,
      decoration: BoxDecoration(
        color: const Color(0xffF5F5F5),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: width! / 32),
        child: Center(
          child: TextField(
            controller: controller,
            keyboardType: type??TextInputType.text,
            cursorColor: CustomColor.primaryLight,
            style: GoogleFonts.inter(
                textStyle: TextStyle(fontSize: double.parse(((width*0.03).toString().contains('.')==true)?(width*0.03).toString().split('.')[0]:(width*0.03).toString()), color: CustomColor.primaryLight)),
            decoration: InputDecoration(
              isDense: true,
              hintText: hintText??'',
              contentPadding: const EdgeInsets.all(0),
              hintStyle: GoogleFonts.inter(
                  textStyle: TextStyle(fontSize: double.parse(((width*0.03).toString().contains('.')==true)?(width*0.03).toString().split('.')[0]:(width*0.03).toString()), color: CustomColor.primaryLight)),
              helperStyle: GoogleFonts.poppins(
                  textStyle: TextStyle(fontSize: double.parse(((width*0.03).toString().contains('.')==true)?(width*0.03).toString().split('.')[0]:(width*0.03).toString()))),
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
          ),
        ),
      ),
    );
  }

  static Widget textFieldPassword({FocusNode? focusNode, bool? bool, TextEditingController? controller, double? height, double? width, GestureDetector? gestureDetector}){
    return Container(
      height: height! / 14,
      decoration: BoxDecoration(
        color: const Color(0xffF5F5F5),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: width! / 32),
        child: Row(
          children: [
            Expanded(
              child: TextField(
                enableInteractiveSelection: false,
                autocorrect: false,
                focusNode: focusNode,
                obscureText: bool!,
                controller: controller,
                cursorColor: CustomColor.primaryLight,
                style: GoogleFonts.poppins(
                    textStyle: TextStyle(fontSize: 14, color: CustomColor.primaryLight)),
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.all(0),
                  hintStyle: GoogleFonts.poppins(
                      textStyle: TextStyle(
                          fontSize: 16, color: CustomColor.primaryLight)),
                  helperStyle: GoogleFonts.poppins(
                      textStyle: const TextStyle(fontSize: 14)),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
              ),
            ),
            gestureDetector!
          ],
        ),
      ),
    );
  }

}