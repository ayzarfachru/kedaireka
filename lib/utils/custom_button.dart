import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_colors.dart';
import 'custom_text.dart';


class CustomButton{


  static Widget googleButton({String? text, double? height,
    double? width, int? maxLines}){
    return Container(
      height: height! / 12,
      width: width,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          border: Border.all(color: CustomColor.primaryLight)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/icon_google.png",
            width: width! / 14,
            height: width / 14,
          ),
          CustomText.bodyMedium16(
            text: " Masuk dengan Google",
            maxLines: 1,
            color: Colors.black,
            MediaQuery: width,
            // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
          ),
        ],
      ),
    );
  }

  static Widget primaryButton({String? text, double? height,
    double? width, int? maxLines, double? MediaQuery}){
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: CustomColor.primary,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.8),
            spreadRadius: 2,
            blurRadius: 3,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Center(
        child: CustomText.bodyMedium16(
          text: text,
          maxLines: maxLines??1,
          color: Colors.white,
          MediaQuery: MediaQuery,
          // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString())
        ),
      ),
    );
  }

  static Widget primaryButtonLoading({double? height, double? width}){
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          color: CustomColor.primary,
          borderRadius: BorderRadius.circular(20)
      ),
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
          color: CustomColor.primaryLight,
        ),
      ),
    );
  }

}