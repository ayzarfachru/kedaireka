import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kedaireka/ui/role/admin/home/home_admin_activity.dart';
import 'package:kedaireka/ui/role/admin/profile/profile_activity.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:page_transition/page_transition.dart';

import 'custom_size.dart';


class CustomNavbar extends StatefulWidget {
  const CustomNavbar(callback);

  @override
  _CustomNavbarState createState() => _CustomNavbarState();
}

class _CustomNavbarState extends State<CustomNavbar> {
  int indexPage = 0;

  void _changeSelectedNavBar(int index) {
    setState(() {
      indexPage = index;
    });
    print(indexPage);
    if (indexPage == 0) {
      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.fade,
              child: const HomeAdminActivity()));
    } else if (indexPage == 3) {
      Navigator.pushReplacement(
          context,
          PageTransition(
              type: PageTransitionType.fade,
              child: const ProfileActivity()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: indexPage,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: true,
      showUnselectedLabels: true,
      selectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)!*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
      unselectedLabelStyle: TextStyle(fontSize: double.parse(((CustomSize.sizeWidth(context)*0.025).toString().contains('.')==true)?(CustomSize.sizeWidth(context)*0.025).toString().split('.')[0]:(CustomSize.sizeWidth(context)*0.025).toString())),
      selectedItemColor: CustomColor.primaryLight,
      unselectedItemColor: CustomColor.primary,
      onTap: _changeSelectedNavBar,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Beranda',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.water),
          label: 'Kontrol Air',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.amp_stories_rounded),
          label: 'Kontrol Kemasan',
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesome.user),
          label: 'Profil',
        ),
      ],
    );
  }
}