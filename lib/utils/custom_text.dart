import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// 423.5
class CustomText{
  static Widget text(
      {String? text,
        double? size,
        double? minSize,
        int? maxLines,
        FontWeight? weight,
        Color? color}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: size,
              fontWeight:
              weight, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading1({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 32,
              fontSize: minSize,
              fontWeight:
              FontWeight.w700, color: color??=Colors.black)),
      minFontSize: (minSize == 0)?0:0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading2({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 32,
              fontSize: minSize,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget auth({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.ebGaramond(
          textStyle: TextStyle(
            // fontSize: 32,
              fontSize: minSize,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading3({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 24,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading4({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 18,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading4a({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 20,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading5({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 24,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading5a({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 22,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.035).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.035).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.035).toString())
  static Widget textHeading6({String? text, Color? color,
    double? minSize, double? sizeNew, double? maxSize, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      // maxFontSize: maxSize,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading7({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading8({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 28,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textHeading9({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 30,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle1({String? text, Color? color,
    double? minSize, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: minSize,
              fontWeight:
              FontWeight.w400, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle2({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle2c({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 18,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
      textAlign: TextAlign.center,
    );
  }

  static Widget textTitle3({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle5({String? text, Color? color,
    double? minSize, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: minSize,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle6({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w700, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle16a({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: Colors.white,
              fontSize: double.parse(((MediaQuery!*0.04).toString().contains('.')==true)?(MediaQuery*0.04).toString().split('.')[0]:(MediaQuery*0.04).toString()),
              fontWeight: FontWeight.w700,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle24({String? text, Color? color, String? bg,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: double.parse(((MediaQuery!*0.057).toString().contains('.')==true)?(MediaQuery*0.057).toString().split('.')[0]:(MediaQuery*0.057).toString()),
              fontWeight: FontWeight.w700,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle7({String? text, Color? color,
    double? minSize, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
              fontSize: minSize,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }


  // static Widget textTitle16({String? text, Color? color,
  //   double? minSize, double? sizeNew, int? maxLines,BuildContext? context}){
  //   return AutoSizeText(
  //     text!,
  //     style: GoogleFonts.inter(
  //         textStyle: TextStyle(
  //           // fontSize: 14,
  //             fontSize: double.parse(((MediaQuery.of(context!).size.width*0.04).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.04).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.04).toString()),
  //             fontWeight: FontWeight.w600,
  //             color: color??Colors.black)),
  //     minFontSize: minSize??0,
  //     maxLines: maxLines??1,
  //   );
  // }
  static Widget textTitle12({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: double.parse(((MediaQuery!*0.03).toString().contains('.')==true)?(MediaQuery*0.03).toString().split('.')[0]:(MediaQuery*0.03).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle15({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: double.parse(((MediaQuery!*0.037).toString().contains('.')==true)?(MediaQuery*0.037).toString().split('.')[0]:(MediaQuery*0.037).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle16({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
            backgroundColor: Colors.white,
              fontSize: double.parse(((MediaQuery!*0.04).toString().contains('.')==true)?(MediaQuery*0.04).toString().split('.')[0]:(MediaQuery*0.04).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle18({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
            backgroundColor: Colors.white,
              fontSize: double.parse(((MediaQuery!*0.043).toString().contains('.')==true)?(MediaQuery*0.043).toString().split('.')[0]:(MediaQuery*0.043).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget textTitle20({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
            backgroundColor: Colors.white,
              fontSize: double.parse(((MediaQuery!*0.0475).toString().contains('.')==true)?(MediaQuery*0.0475).toString().split('.')[0]:(MediaQuery*0.0475).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium16a({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines, TextDecoration? decoration, TextAlign? textAlign,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black,
              decoration: decoration??TextDecoration.none)),
      textAlign: TextAlign.left,
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium16b({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines, TextDecoration? decoration, TextAlign? textAlign,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black,
              decoration: decoration??TextDecoration.none)),
      textAlign: TextAlign.center,
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium16c({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines, TextDecoration? decoration, TextAlign? textAlign,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black,
              decoration: decoration??TextDecoration.none)),
      textAlign: TextAlign.right,
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium14({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.035).toString().contains('.')==true)?(MediaQuery*0.035).toString().split('.')[0]:(MediaQuery*0.035).toString()),
              fontWeight: FontWeight.w500,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium12({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.03).toString().contains('.')==true)?(MediaQuery*0.03).toString().split('.')[0]:(MediaQuery*0.03).toString()),
              fontWeight: FontWeight.w500,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium10({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.025).toString().contains('.')==true)?(MediaQuery*0.025).toString().split('.')[0]:(MediaQuery*0.025).toString()),
              fontWeight: FontWeight.w500,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyMedium16({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: double.parse(((MediaQuery!*0.04).toString().contains('.')==true)?(MediaQuery*0.04).toString().split('.')[0]:(MediaQuery*0.04).toString()),
              fontWeight: FontWeight.w500,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  // sizeNew: double.parse(((MediaQuery.of(context).size.width*0.038).toString().contains('.')==true)?(MediaQuery.of(context).size.width*0.038).toString().split('.')[0]:(MediaQuery.of(context).size.width*0.038).toString())
  static Widget bodyRegular18({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 15,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w600, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular17({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 15,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w500, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular16({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w400, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular14({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: double.parse(((MediaQuery!*0.035).toString().contains('.')==true)?(MediaQuery*0.035).toString().split('.')[0]:(MediaQuery*0.035).toString()),
              fontWeight: FontWeight.w600,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular12({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.03).toString().contains('.')==true)?(MediaQuery*0.03).toString().split('.')[0]:(MediaQuery*0.03).toString()),
              fontWeight: FontWeight.w500,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular10({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines, TextDecoration? decoration}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 10,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w400, color: color??=Colors.black,
              decoration: decoration??TextDecoration.none)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular14a({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.035).toString().contains('.')==true)?(MediaQuery*0.035).toString().split('.')[0]:(MediaQuery*0.035).toString()),
              fontWeight: FontWeight.w400,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyRegular15({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,double? MediaQuery, Color? bgColor}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              backgroundColor: bgColor??Colors.transparent,
              fontSize: double.parse(((MediaQuery!*0.037).toString().contains('.')==true)?(MediaQuery*0.037).toString().split('.')[0]:(MediaQuery*0.037).toString()),
              fontWeight: FontWeight.w400,
              color: color??Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyLight16({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines, TextDecoration? decoration}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 16,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black,
              decoration: decoration??TextDecoration.none)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyLight14({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 14,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyLight12({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 12,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }

  static Widget bodyLight10({String? text, Color? color,
    double? minSize, double? sizeNew, int? maxLines,}){
    return AutoSizeText(
      text!,
      style: GoogleFonts.inter(
          textStyle: TextStyle(
            // fontSize: 10,
              fontSize: sizeNew,
              fontWeight:
              FontWeight.w300, color: color??=Colors.black)),
      minFontSize: minSize??0,
      maxLines: maxLines??1,
    );
  }
}