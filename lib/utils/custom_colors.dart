import 'dart:ui';

class CustomColor{
  static Color primary = Color(0xff4b3220);
  static Color primaryLight = Color(0xff776d59);
  static Color primaryLight2 = Color(0xffd5d4c1);
  // static Color primaryLight = Color(0xffAF1E22).withOpacity(.2);
  static Color secondary = Color(0xffd9d9d9);
  static Color black = Color(0xff000000);

  static Color accent = Color(0xff26CD67);
  static Color accentLight = Color(0xff26CD67).withOpacity(0.2);

  static Color redBtn = Color.fromRGBO(251,22,10,1);

  static Color background = Color(0xffF2F6FD);

  static Color textTitle = Color(0xff040507);
  static Color textBody = Color(0xffA5A5A5);
  static Color textBodyWhite = Color(0xffFFFFFF);

  static Color dividerDark = Color(0xffCBD0DF);
  static Color dividerLight = Color(0xffF4F6FA);

  static List<Color> gradient1 = [Color(0xff2BC5F1), Color(0xff7FE2FE)];
  static List<Color> gradient2 = [Color(0xffFDFDFD), Color(0xffD6E7F3)];
  static List<Color> gradient3 = [Color(0xffFF92B0), Color(0xffD274DE)];
  static List<Color> gradient4 = [Color(0xff6EDBFA), Color(0xffBDF0FF)];
}