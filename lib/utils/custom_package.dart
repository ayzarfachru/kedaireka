import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kedaireka/ui/role/admin/input/bahan_baku/add_supply.dart';
import 'package:kedaireka/ui/role/admin/input/esn/esn_kotor.dart';
import 'package:kedaireka/ui/role/admin/input/kontrol_air_produksi/add_air_kimiawi_before.dart';
import 'package:kedaireka/ui/role/user/detail/detail_esn.dart';
import 'package:kedaireka/ui/splash_screen/splash_screen.dart';
import 'package:kedaireka/utils/custom_colors.dart';
import 'package:kedaireka/utils/custom_text.dart';
import 'package:page_transition/page_transition.dart';


class CustomPackage{


  static Widget imageDetailHomeUser ({String? label, String? text, bool? image, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return Container(
      width: width! / 1.1,
      height: height! / 5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 0), // changes position of shadow
          ),
        ],
      ),
    );
  }

  static Widget imageDetailSquare ({String? label, String? text, int? image, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return SizedBox(
      height: width! / 2.5,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: image,
          itemBuilder: (_, index){
          return Padding(
            padding: EdgeInsets.only(left: width * 0.015, right: width * 0.03, top: width * 0.015, bottom: width * 0.015),
            child: Container(
              width: width! / 2.5,
              height: width / 2.5,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: const Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
            ),
          );
        }
      ),
    );
  }


  static Widget menuHomeUser ({String? label, String? text,  String? page, bool? image, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return Container(
      width: width! / 1.1,
      height: height! / 5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 2,
            offset: const Offset(0, 0), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            alignment: Alignment.topRight,
            children: [
              SizedBox(
                width: width / 1.1,
                height: height / 7,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width / 24, vertical: width / 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText.textTitle16a(
                        text: label,
                        color: CustomColor.primary,
                        MediaQuery: width,
                      ),
                      SizedBox(height: height * 0.005),
                      CustomText.bodyMedium14(
                        text: text,
                        color: CustomColor.primary,
                        MediaQuery: width,
                      ),
                    ],
                  ),
                ),
              ),
              (image == true)?Image.asset(
                "assets/circle_1.png",
                fit: BoxFit.fill,
                width: width / 5,
                height: width / 5,
              ):Container(),
              (image == true)?Positioned(
                top: 18,
                child: ClipRect(
                  child: Align(
                    alignment: Alignment.topLeft,
                    widthFactor: 0.9,
                    heightFactor: 0.8,
                    child: Image.asset(
                      "assets/circle_2.png",
                      fit: BoxFit.fill,
                      width: width / 3.5,
                      height: width / 3.5,
                    ),
                  ),
                ),
              ):Container(),
            ],
          ),
          GestureDetector(
            onTap: (){
              if (page == '1') {

              } else if (page == '2') {
                Navigator.push(
                    context!,
                    PageTransition(
                        type: PageTransitionType.theme,
                        alignment: Alignment.center,
                        duration: const Duration(milliseconds: 700),
                        child: const DetailEsnActivity()));
              } else if (page == '3') {

              }
            },
            child: Container(
              width: width / 1.1,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  top: BorderSide( //                    <--- top side
                    color: CustomColor.secondary,
                    width: 1.0,
                  ),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.only(left: width / 24, right: width / 24, top: height * 0.0175, bottom: height * 0.0075),
                child: CustomText.bodyRegular14a(
                  text: "Lihat detailnya >",
                  color: Colors.black,
                  MediaQuery: width,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


  static Widget menuHomeAdmin ({String? text, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return Container(
      width: width! / 1.1,
      height: height! / 5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(25),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 7,
            offset: const Offset(0, 7), // changes position of shadow
          ),
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          CustomText.textTitle24(
            text: "-----------------------------------",
            color: CustomColor.primaryLight,
            MediaQuery: width,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width / 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        child: CustomText.textTitle16(
                          text: "Total Suplai",
                          color: CustomColor.primaryLight,
                          MediaQuery: width,
                        )
                    ),
                    Row(
                      children: [
                        CustomText.textTitle24(
                          text: "240",
                          color: Colors.black,
                          MediaQuery: width,
                        ),
                        CustomText.textTitle24(
                          text: " kg",
                          color: Colors.black,
                          MediaQuery: width,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width / 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(
                            context!,
                            PageTransition(
                                type: PageTransitionType.theme,
                                alignment: Alignment.center,
                                duration: const Duration(milliseconds: 700),
                                child: AddSupply()));
                      },
                      child: Column(
                        children: [
                          Icon(Icons.note_add_rounded, color: CustomColor.primary, size: 28,),
                          CustomText.bodyMedium12(
                            text: "Tambah",
                            color: Colors.black,
                            MediaQuery: width,
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(
                            context!,
                            PageTransition(
                                type: PageTransitionType.theme,
                                alignment: Alignment.center,
                                duration: const Duration(milliseconds: 700),
                                child: AddAirKimiawiB()));
                      },
                      child: Column(
                        children: [
                          Icon(Icons.water, color: CustomColor.primary, size: 28,),
                          CustomText.bodyMedium12(
                            text: "Air",
                            color: Colors.black,
                            MediaQuery: width,
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Icon(Icons.amp_stories_rounded , color: CustomColor.primary, size: 28,),
                        CustomText.bodyMedium12(
                          text: "Kemasan",
                          color: Colors.black,
                          MediaQuery: width,
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pushReplacement(
                            context!,
                            PageTransition(
                                type: PageTransitionType.theme,
                                alignment: Alignment.center,
                                duration: const Duration(milliseconds: 700),
                                child: AddEsnKotor()));
                      },
                      child: Column(
                        children: [
                          Icon(Icons.fact_check_rounded , color: CustomColor.primary, size: 28,),
                          CustomText.bodyMedium12(
                            text: "Siap Jual",
                            color: Colors.black,
                            MediaQuery: width,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  static Widget listBahanBaku ({String? text, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 3,
        itemBuilder: (_, index){
        return ListView(
          physics: const NeverScrollableScrollPhysics(),
          padding: EdgeInsets.zero,
          shrinkWrap: true,
          children: [
            SizedBox(height: height! * 0.0075,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                        width: width! / 5,
                        height: width / 5,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(20),
                        )
                    ),
                    SizedBox(width: width / 46,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: width / 2.8,
                          child: CustomText.bodyMedium10(
                            maxLines: 2,
                            text: text,
                            MediaQuery: width,
                          ),
                        ),
                        RatingBar.builder(
                          ignoreGestures: true,
                          itemSize: 12,
                          initialRating: 5,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => const Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            if (kDebugMode) {
                              print(rating);
                            }
                          },
                        )
                      ],
                    ),
                  ],
                ),
                Container(
                  height: height / 24,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: CustomColor.primaryLight2
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: width / 38),
                    child: Center(
                      child: Row(
                        children: [
                          Icon(Icons.outbond_outlined, color: CustomColor.primary, size: 16,),
                          CustomText.bodyMedium10(
                            text: " Lihat Detail",
                            color: CustomColor.primary,
                            MediaQuery: width,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      }
    );
  }

  static Widget hasilUji ({String? text, double? height,
    double? width, int? maxLines, BuildContext? context}){
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          padding: EdgeInsets.zero,
          alignment: Alignment.center,
          width: width,
          height: height! / 7.5,
          decoration: BoxDecoration(
            color: CustomColor.primary,
          ),
        ),
        Image.asset(
          "assets/wave.png",
          fit: BoxFit.fill,
          width: width! / 1,
          height: height / 7.5,
        ),
        Container(
          alignment: Alignment.center,
          width: width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: width / 1.8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: width,
                      child: CustomText.bodyMedium16(
                        maxLines: 2,
                        text: 'Kontrol Kualitas Air',
                        color: Colors.white,
                        MediaQuery: width,
                      ),
                    ),
                    SizedBox(height: height * 0.015,),
                    SizedBox(
                      width: width / 2,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            child: CustomText.bodyRegular12(
                              maxLines: 2,
                              text: 'Hasil Uji Kimiawi',
                              color: Colors.white,
                              MediaQuery: width,
                            ),
                          ),
                          RatingBar.builder(
                            ignoreGestures: true,
                            itemSize: 14,
                            initialRating: 5,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              if (kDebugMode) {
                                print(rating);
                              }
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width / 2,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            child: CustomText.bodyRegular12(
                              maxLines: 2,
                              text: 'Hasil Uji Mikrobiologi',
                              color: Colors.white,
                              MediaQuery: width,
                            ),
                          ),
                          RatingBar.builder(
                            ignoreGestures: true,
                            itemSize: 14,
                            initialRating: 5,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              if (kDebugMode) {
                                print(rating);
                              }
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: width * 0.025),
              Container(
                height: height / 24,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: CustomColor.primaryLight2
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: width / 38),
                  child: Center(
                    child: Row(
                      children: [
                        Icon(Icons.outbond_outlined, color: CustomColor.primary, size: 16,),
                        CustomText.bodyMedium10(
                          text: " Lihat Detail",
                          color: CustomColor.primary,
                          MediaQuery: width,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}